/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare module '!!prismjs-loader*' {
  const contents: string;
  export = contents;
}

declare module '*.html' {
  const contents: string;
  export = contents;
}

declare module '*.json' {
  const contents: string;
  export = contents;
}
