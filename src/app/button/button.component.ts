import { Component, OnInit } from '@angular/core';

import { DATA } from './button.data';
import { GuideTabModel } from '../share';

import * as HTML from '!!prismjs-loader?lang=markup!./session/default/default.component.html';
import * as TS from '!!prismjs-loader?lang=typescript!./session/default/default.component.ts';
import * as CSS from '!!prismjs-loader?lang=css!./session/default/default.component.scss';


import * as HTMLmagic from '!!prismjs-loader?lang=markup!./session/magic/magic.component.html';
import * as TSmagic from '!!prismjs-loader?lang=typescript!./session/magic/magic.component.ts';
import * as CSSmagic from '!!prismjs-loader?lang=css!./session/magic/magic.component';

@Component({
  selector: 'demo-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  public data = DATA;

  public tabData: GuideTabModel[] = [
    {
      active: true,
      title: 'HTML',
      content: HTML,
      type: 'language-html'
    },
    {
      title: 'TS',
      content: TS,
      type: 'language-typescript'
    },
    {
      title: 'css',
      content: CSS,
      type: 'language-css'
    }
  ];

  public tabDataMagic: GuideTabModel[] = [
    {
      active: true,
      title: 'HTML',
      content: HTMLmagic,
      type: 'language-html'
    },
    {
      title: 'TS',
      content: TSmagic,
      type: 'language-typescript'
    },
    {
      title: 'css',
      content: CSSmagic,
      type: 'language-css'
    }
  ];

  public listTabData = [
    this.tabData,
    this.tabDataMagic
  ];

  constructor() { }

  ngOnInit() {
  }
}
