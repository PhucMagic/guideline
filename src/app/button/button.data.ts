export const DATA = {
  name: 'List-View',
  description: 'The List View control allows users to pick strings from lists.',
  selector: 'mjs-ui-list-view',
  component: 'ListViewComponent',
  overviews: [
    {
      name: '',
      description: 'Gets or sets the array or custom object that contains the items to select from.'
    },
    {
      name: 'Selecter: ',
      description: 'app-content'
    },
    {
      name: 'Compoent: ',
      description: 'ContentComponent'
    }
  ],
  properties: [
    {
      name: 'itemsSource',
      description: 'Gets or sets the array or custom object that contains the items to select from.',
      type: 'any'
    },
    {
      name: 'displayMemberPath',
      description: 'Gets or sets the name of the property to use as the visual representation of the items.',
      type: 'string'
    },
    {
      name: 'selectedValuePath',
      description: 'Gets or sets the name of the property used to get the selectedValue from the selectedItem.',
      type: 'string'
    },
    {
      name: 'headerName',
      description: 'Gets or sets header name of list view.',
      type: 'string'
    },
    {
      name: 'isShowHeader',
      description: 'Show or hide Header.',
      type: 'boolean',
      default: true
    },
    {
      name: 'isMultiSelect',
      description: 'Set mode multiple select or single select.',
      type: 'boolean',
      default: false
    },
    {
      name: 'numberStringLimitOfTitle',
      description: 'Set number string limit of title',
      type: 'number',
      default: 500
    },
    {
      name: 'isWrapHeaderText',
      description: 'Header text is word wrap.',
      type: 'boolean',
      default: false
    },
    {
      name: 'positionOfTitle',
      description: 'Set position of header text. Values: center/right/left',
      type: 'string',
      default: 'center'
    },
    {
      name: 'sort',
      description: 'Enabled sort when click header.',
      type: 'boolean',
      default: 'false'
    }
  ],
  methods: [
    {
      name: 'getSelectedItems',
      syntax: 'getSelectedItems()',
      description: 'get selected items'
    },
    {
      name: 'focus',
      syntax: 'focus()',
      description: 'focus on control'
    }
  ],
  events: [
    {
      name: 'selectedIndexChanged',
      description: 'Occurs when the item in the list is changed, either as a result of user actions or by assignment in code.',
      args: 'EventArgs'
    },
    {
      name: 'lostFocus',
      description: 'Occurs when control lost focus.'
    },
    {
      name: 'gotFocus',
      description: 'Occurs when control got focus.'
    }
  ]
};
