import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ButtonComponent } from './button.component';
import { ShareModule } from '../share/share.module';
import { DefaultComponent } from './session/default/default.component';
import { MagicComponent } from './session/magic/magic.component';

const routing: Routes = [{ path: '', component: ButtonComponent }];

@NgModule({
    declarations: [
        ButtonComponent,
        DefaultComponent,
        MagicComponent
        ],
    imports: [
        CommonModule,
        ShareModule,
        RouterModule.forChild(routing),
    ]
})
export class ButtonModule { }
