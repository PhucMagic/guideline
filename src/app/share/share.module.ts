import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import {
    ContentGuidelineModule,
    SessionGuidelineModule
} from '.';

const UI_MODULES = [
    ContentGuidelineModule,
    SessionGuidelineModule
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        HttpModule
    ],
    exports: [
        ...UI_MODULES
    ],
    entryComponents: [],
    declarations: []
})
export class ShareModule { }
