import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GuideTabModel } from './guide-tab.model';
import { SessionGuidelineComponent } from './session-guideline.component';
@NgModule({
  declarations: [SessionGuidelineComponent],
  imports: [CommonModule],
  exports: [SessionGuidelineComponent],
  providers: []
})
export class SessionGuidelineModule { }
