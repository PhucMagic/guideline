import {
  Component,
  OnInit,
  Input
} from '@angular/core';

import { GuideTabModel } from './guide-tab.model';

@Component({
  selector: 'session-guideline',
  templateUrl: './session-guideline.component.html',
  styleUrls: ['./session-guideline.component.scss']
})

export class SessionGuidelineComponent implements OnInit {
  @Input('title') title: any;
  @Input('tab') tab: Array<GuideTabModel>;
  constructor() {
  }
  ngOnInit() {
  }

  public selectedTab(item: GuideTabModel): void {
    this.tab.forEach(element => {
      element.active = false;
    });
    item.active = true;
  }
}
