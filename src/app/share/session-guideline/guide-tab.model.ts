export class GuideTabModel {
    title: String;
    content: any;
    active?: boolean;
    type?: String;
}

