import {
  Component,
  OnInit,
  Input
} from '@angular/core';

@Component({
  selector: 'content-guideline',
  templateUrl: './content-guideline.component.html',
  styleUrls: ['./content-guideline.component.scss']
})
export class ContentGuidelineComponent implements OnInit {

  @Input('data') data: any;

  constructor() { }

  ngOnInit() {
  }

}
