import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ContentGuidelineComponent } from './content-guideline.component';

@NgModule({
    declarations: [ContentGuidelineComponent],
    imports: [CommonModule],
    exports: [ContentGuidelineComponent]
})
export class ContentGuidelineModule { }
