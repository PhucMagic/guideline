import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { ButtonComponent } from './button/button.component';


const appRoutes: Routes = [
    //   { path: '', redirectTo: 'home', pathMatch: 'full' },
    // {
    //     path: 'checkbox',
    //     loadChildren: './checkbox/checkbox.module#DemoCheckboxModule'
    // },
    {
        path: 'button',
        loadChildren: './button/button.module#ButtonModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
